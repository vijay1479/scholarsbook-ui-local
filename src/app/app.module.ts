import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule ,Routes} from '@angular/router'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ConfirmEqualValidatorDirective } from '../shared/confirm-equal-validator.directive';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

const appRoutes: Routes = [
                           {path:'home', component:NavBarComponent,
                          
                           children:[

                             {path: '',component: DashboardComponent},
                             {path: 'profile',component: ProfileComponent}
                          
                          
                                 ]
                          
                            },
                           {path: 'login',component:LoginComponent},
                           {path: '',redirectTo:'/login',pathMatch: 'full'},
                          ];

@NgModule({
  declarations: [
    AppComponent,
    ConfirmEqualValidatorDirective,
   LoginComponent,
    DashboardComponent,
    ProfileComponent,
    NavBarComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
