import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import { UserService } from '../services/user.service';
import { User } from '../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[UserService]
})
export class LoginComponent implements OnInit {

  ngOnInit() {
  }

  constructor(
    private userService: UserService , private router: Router){ }

    private users:User[];

        newUser:User = {
                        "id":null,
                        "firstname": null,
                        "lastname": null,
                        "mobile":null,
                        "email": null,
                        "address": null,
                        "password": null,
                        "birthday": -1,
                        "birthmonth":"Month",
                        "birthyear":-1,
                        "gender": "male",
                        "isActive": false
  
                       };

        days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26
                ,27,28,29,30,31];
        months = ["Jan","Feb","March","April","May","June","July","August","Sep","Oct","Nov","Dec"];
        years  = [1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000];
        success:boolean = false;
        count_user:number = 0;
    addUsers():void{
     
      this.userService.addUser(this.newUser)
      .subscribe(success => {this.success=true});

    }

    login():void{

      this.userService.loginUser()
      .subscribe(user => {this.count_user = user.length;
              if(this.count_user>0){

                                  this.router.navigate(['/home'])

                                   }
                         },
              err => console.error('Observer got an error: ' + err),
              () => console.log('Observer got a complete notification')              
                        
                        );
      
     
    }
}
