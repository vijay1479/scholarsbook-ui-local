export class User {
    constructor(
        public id:number,
        public firstname: string, 
        public lastname:string,
        public mobile:string,
        public email:string,
        public address:string,
        public password:string,
        public birthday:number,
        public birthmonth:string,
        public birthyear:number,
        public gender:string,
        public isActive:boolean
        ){}
}