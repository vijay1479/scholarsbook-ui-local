import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  toggle:boolean = false;
  toggle_settings(){

    this.toggle = !this.toggle;
    
  }
}
