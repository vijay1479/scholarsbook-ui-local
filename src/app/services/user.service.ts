import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class UserService{
  
    
 usersUrl: string="http://localhost:3000/user";   
 constructor(private http: HttpClient) { }
    
 addUser (user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user, {headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })})
      .pipe(
        catchError(this.handleError)
      );
  }

 loginUser():Observable<User[]>{

              return this.http.get<User[]>('http://localhost:3000/user').pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
              );


        }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
